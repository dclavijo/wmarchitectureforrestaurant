﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace Servicios.Entidades.Entrada
{
    public class ECustomer
    {
        public ECustomer()
        {
            Orders = new HashSet<EOrder>();
        }
        
        public int CustomerId { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }

        public virtual ICollection<EOrder> Orders { get; set; }
    }
}
