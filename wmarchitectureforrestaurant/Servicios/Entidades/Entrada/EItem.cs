﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace Servicios.Entidades.Entrada
{
    public class EItem
    {
        public EItem()
        {
            OrderItems = new HashSet<EOrderItem>();
        }
        [Required]
        public int ItemId { get; set; }
        [Required, MaxLength(50)]
        public string Name { get; set; }
        [Required]
        public decimal? Price { get; set; }

        public virtual ICollection<EOrderItem> OrderItems { get; set; }
    }
}
