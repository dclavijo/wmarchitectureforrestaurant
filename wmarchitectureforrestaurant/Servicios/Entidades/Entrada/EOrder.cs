﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace Servicios.Entidades.Entrada
{
    public class EOrder
    {
        public EOrder()
        {
            OrderItems = new HashSet<EOrderItem>();
        }

        [Required]
        public long OrderId { get; set; }
        [Required]
        public string OrderNo { get; set; }
        [Required]
        public int? CustomerId { get; set; }
        [Required, MaxLength(50)]
        public string Pmethod { get; set; }
        public decimal? Gtotal { get; set; }

        public virtual ECustomer Customer { get; set; }
        public virtual ICollection<EOrderItem> OrderItems { get; set; }
    }
}
