﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace Servicios.Entidades.Entrada
{
    public class EOrderItem
    {
        [Required]
        public long OrderItemId { get; set; }
        [Required]
        public long? OrderId { get; set; }
        [Required]
        public int? ItemId { get; set; }
        public int? Quantity { get; set; }

        public virtual EItem Item { get; set; }
        public virtual EOrder Order { get; set; }
    }
}
