﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Servicios.Entidades.Salida
{
    public partial class SCustomer
    {
        public SCustomer()
        {
            Orders = new HashSet<SOrder>();
        }

        public int CustomerId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<SOrder> Orders { get; set; }
    }
}
