﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Servicios.Entidades.Salida
{
    public partial class SItem
    {
        public SItem()
        {
            OrderItems = new HashSet<SOrderItem>();
        }

        public int ItemId { get; set; }
        public string Name { get; set; }
        public decimal? Price { get; set; }

        public virtual ICollection<SOrderItem> OrderItems { get; set; }
    }
}
