﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Servicios.Entidades.Salida
{
    public partial class SOrder
    {
        public SOrder()
        {
            OrderItems = new HashSet<SOrderItem>();
        }

        public long OrderId { get; set; }
        public string OrderNo { get; set; }
        public int? CustomerId { get; set; }
        public string Pmethod { get; set; }
        public decimal? Gtotal { get; set; }

        public virtual SCustomer Customer { get; set; }
        public virtual ICollection<SOrderItem> OrderItems { get; set; }
    }
}
