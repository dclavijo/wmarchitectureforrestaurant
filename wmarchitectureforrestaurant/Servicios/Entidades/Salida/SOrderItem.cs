﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Servicios.Entidades.Salida
{
    public partial class SOrderItem
    {
        public long OrderItemId { get; set; }
        public long? OrderId { get; set; }
        public int? ItemId { get; set; }
        public int? Quantity { get; set; }

        public virtual SItem Item { get; set; }
        public virtual SOrder Order { get; set; }
    }
}
