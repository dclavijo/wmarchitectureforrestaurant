﻿using AccesoDatos.Entidades;
using Service.Common.Collection;
using Servicios.Entidades.Salida;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interfaces
{
    public interface ICustomerService
    {

        Task<Customer> GetCustomer(int id);

        Task InsertPostCustomer(Customer post);

        Task<bool> UpdateCustomer(Customer post);

        Task<bool> DeleteCustomer(int id);


        Task<DataCollection<SCustomer>> GetAllAsync(int page, int take, IEnumerable<int> Customers = null);

        Task<SCustomer> GetAsync(int id);



    }
}
