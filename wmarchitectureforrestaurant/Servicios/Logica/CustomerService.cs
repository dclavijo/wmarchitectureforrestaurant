﻿using AccesoDatos.Contexto;
using AccesoDatos.Entidades;
using Microsoft.EntityFrameworkCore;
using Service.Common.Collection;
using Service.Common.Mapping;
using Service.Common.Paging;
using Servicios.Entidades.Salida;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Logica
{
    public class CustomerService : ICustomerService
    {
        private readonly RestaurantDBContext _context;

        public CustomerService(
            RestaurantDBContext context)
        {
            _context = context;
        }

        public Task<bool> DeleteCustomer(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<DataCollection<SCustomer>> GetAllAsync(int page, int take, IEnumerable<int> Customers = null)
        {
            var collection = await _context.Customers
                .Where(x => Customers == null || Customers.Contains(x.CustomerId))
                .OrderBy(x => x.Name)
                .GetPagedAsync(page, take);

            return collection.MapTo<DataCollection<SCustomer>>();
        }

        public async Task<SCustomer> GetAsync(int id)
        {
            return (await _context.Customers.SingleAsync(x => x.CustomerId == id)).MapTo<SCustomer>();
        }

        public Task<Customer> GetCustomer(int id)
        {
            throw new NotImplementedException();
        }

        public Task InsertPostCustomer(Customer post)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateCustomer(Customer post)
        {
            throw new NotImplementedException();
        }
    }
}
