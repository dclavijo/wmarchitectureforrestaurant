﻿using AutoMapper;
using Servicios.Entidades.Entrada;
using Servicios.Entidades.Salida;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Mapeo
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {

            CreateMap<SCustomer, ECustomer>();

        }
    }
}
