﻿using AccesoDatos.Contexto;
using Microsoft.EntityFrameworkCore;

namespace Servicios.Modelo
{
    public class DbServicioContext : RestaurantDBContext
    {
        public DbServicioContext(DbContextOptions<RestaurantDBContext> options) : base(options)
        {
        }

    }
}
