﻿using AccesoDatos.Contexto;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Service.Common.Collection;
using Servicios.Entidades.Salida;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        private readonly IMapper _mapper;
        private readonly RestaurantDBContext _context;

        public CustomerController(IMapper mapper, ICustomerService customerService)
        {
            _customerService = customerService;
            _mapper = mapper;
        }


        [HttpGet]
        public async Task<DataCollection<SCustomer>> GetAll(int page = 1, int take = 10, string ids = null)
        {
            IEnumerable<int> customers = null;

            if (!string.IsNullOrEmpty(ids))
            {
                customers = ids.Split(',').Select(x => Convert.ToInt32(x));
            }

            return await _customerService.GetAllAsync(page, take, customers);
        }

        [HttpGet("{id}")]
        public async Task<SCustomer> Get(int id)
        {
            return await _customerService.GetAsync(id);
        }


    }
}
